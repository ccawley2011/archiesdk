ARCHIECC = $(ARCHIESDK)/tools/bin/arm-archie-gcc
ARCHIEAS = $(ARCHIESDK)/tools/bin/arm-archie-as
ARCHIEAR = $(ARCHIESDK)/tools/bin/arm-archie-ar
ARCHIEOBJCOPY = $(ARCHIESDK)/tools/bin/arm-archie-objcopy
ARCHIEZIP = $(ARCHIESDK)/tools/bin/arm-archie-zip

# Default GCC flags. Do not remove -mno-thumb-interwork !!!
# I highly recommend that you keep -Wdouble-promotion and -Wfloat-conversion (Check out the README for more info.)
# Feel free to remove -Wall and -Wextra though. That's just me being pedantic
CFLAGS = -mno-thumb-interwork -Wdouble-promotion -Wfloat-conversion -Wall -Wextra

# Default values, feel free to overwrite those in your makefile
APPNAME = App
CSRC = *.c
LIBS = 

# Hostfs path, i've left my path as an example for WSL users
#ARCHIECOPYPATH=/mnt/d/Downloads/Arculator_V2.2_Windows/hostfs
ARCHIECOPYPATH=.
