#!/bin/bash
set -e

echo Creating build folders...
mkdir -p build

echo Preparing variables...
export TOOLSPATH=$(realpath tools/)
export BUILDPATH=$(realpath build/)
export ARCHIESDK=$(realpath .)

echo Downloading and patching Info-ZIP...
cd $BUILDPATH
if [ ! -f unzip60.tar.gz ]; then
	rm -f unzip60.tar.gz.tmp
	wget -O unzip60.tar.gz.tmp https://sourceforge.net/projects/infozip/files/UnZip%206.x%20%28latest%29/UnZip%206.0/unzip60.tar.gz/download
	mv unzip60.tar.gz.tmp unzip60.tar.gz
	tar xvf unzip60.tar.gz
fi
if [ ! -f zip30.tar.gz ]; then
	rm -f zip30.tar.gz.tmp
	wget -O zip30.tar.gz.tmp https://sourceforge.net/projects/infozip/files/Zip%203.x%20%28latest%29/3.0/zip30.tar.gz/download
	mv zip30.tar.gz.tmp zip30.tar.gz
	tar xvf zip30.tar.gz
	(cd zip30 && patch -p1 -i $ARCHIESDK/SDK/zip-add-forriscos-support.diff)
fi

echo Making unzip...
make -C unzip60 -f unix/Makefile generic LOCAL_UNZIP=-DACORN_FTYPE_NFS
cp unzip60/unzip $TOOLSPATH/bin/arm-archie-unzip

echo Making zip...
make -C zip30 -f unix/Makefile generic LOCAL_ZIP=-DFORRISCOS
cp zip30/zip $TOOLSPATH/bin/arm-archie-zip

# Add an empty line before and after final message to make it more visible
echo
echo All done!
echo
