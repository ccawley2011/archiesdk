#ifndef STDLIB_H_
#define STDLIB_H_

#include <decls/NULL.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

#define	RAND_MAX    (int)2147483647

typedef unsigned int size_t;
typedef struct {
    int quot;           /* Quotient.  */
    int rem;            /* Remainder.  */
} div_t;

float atof(const char *str);
float strtod(const char *str, char **endptr);
int atoi(const char *str);

void abort(void);
void exit(int status);
int atexit(void (*func)(void));

int abs(int x);
long int labs(long int x);
long long int llabs(long long int x);

div_t div(int numer, int denom);

int rand(void);
void srand(unsigned int seed);

void* malloc(size_t size);
void* calloc(size_t nitems, size_t size);
void* realloc(void *ptr, size_t size);
void free(void* ptr);

//TBD, signatures here for libgcov
char* getenv(const char* name);

#endif // STDLIB_H_
