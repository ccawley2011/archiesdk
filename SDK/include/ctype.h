#ifndef CTYPE_H_
#define CTYPE_H_

#include "stdbool.h"

int iscntrl(int character);
int isprint(int character);
int isspace(int character);
int isblank(int character);
int isgraph(int character);
int ispunct(int character);
int isalnum(int character);
int isalpha(int character);
int isupper(int character);
int islower(int character);
int isdigit(int character);
int isxdigit(int character);

int tolower(int character);
int toupper(int character);

#endif // CTYPE_H_