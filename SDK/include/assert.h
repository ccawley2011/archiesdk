#ifndef ASSERT_H_
#define ASSERT_H_

#ifdef NDEBUG
#define assert(ignore) ((void)0)
#else
#define assert(EX) (void)((EX) || (__assert(#EX, __FILE__, __LINE__),0))
#endif

void __assert(const char* msg, const char* file, unsigned int line);

#endif // ASSERT_H_