#ifndef STDIO_H_
#define STDIO_H_
#include <stdarg.h>
#include <decls/FILE.h>
#include <decls/size_t.h>
#include <decls/NULL.h>

int puts(const char* string);
int putchar(int character);
int printf(const char* fmt, ...);
int sprintf(char* string, const char* fmt, ...);

int getchar(void);
char* gets(char* str);


//TBD, signatures here to allow libgcc/libgcov to build with libc support
extern FILE* stdin;
extern FILE* stdout;
extern FILE* stderr;
#define stdin stdin
#define stdout stdout
#define stderr stderr
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

int fflush(FILE* stream);
int fprintf(FILE* stream, const char* fmt, ...);
int vfprintf(FILE* stream, const char* fmt, va_list arg);
int fclose(FILE* stream);
FILE* fopen(const char* filename, const char* mode);
size_t fread(void* ptr, size_t size, size_t nmemb, FILE* stream);
int fseek(FILE* stream, long int offset, int whence);
long int ftell(FILE* stream);
size_t fwrite(const void* ptr, size_t size, size_t nmemb, FILE* stream);
void setbuf(FILE* stream, char* buffer);

#endif // STDIO_H_