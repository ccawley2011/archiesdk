#ifndef TIME_H_
#define TIME_H_

#include <stdint.h>

#define CLOCKS_PER_SEC 100
#define TIME_UTC 1

typedef int64_t clock_t;
typedef int64_t time_t;
struct timespec {
    time_t tv_sec;
    long tv_nsec;
};

struct tm {
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday;
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
    int tm_isdst;
};

clock_t clock(void);
double difftime(time_t time_end, time_t time_beg);
time_t mktime(struct tm* timeptr);
time_t time(time_t* timer);
char* asctime(const struct tm* timeptr);
char* ctime(const time_t* timer);
struct tm* gmtime(const time_t* timer);
struct tm* localtime(const time_t* timer);

//todo? here for libgcc



#endif // TIME_H_