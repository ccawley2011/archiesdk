#include "archie/keyboard.h"
#include "archie/SDKTypes.h"
#include "archie/SWI.h"

bool k_checkKeypress(u32 key){
    u32 returnVal;
    key ^= 0xff;
    asm volatile("mov r0, " swiToConst(OSByte_ReadKey) "\n"
                 "mov r1, %1\n"
                 "mov r2, #0xFF\n"
                 "swi " swiToConst(OS_Byte) "\n"
                 "mov %0, r2" : "=r"(returnVal) : "r" (key) : "r0", "r1", "r2", "cc");
    return returnVal;
}