#include <math.h>

#include "archie/SDKTypes.h"

float asin2f(float x){
    float sum, tempExp;
    float factor = 1.0;
    float divisor = 1.0;
    u32 i;
    tempExp = x;
    sum = x;
    for(i = 0; i < 40; ++i) {
        tempExp *= x*x;
        divisor += 2.0f;
        factor *= (2.0f*(float)i + 1.0f)/(((float)i+1.0f)*2.0f);
        sum += factor*tempExp/divisor;
    }
    return sum;
}

float asinf(float x){
    if(fabsf(x) <= 0.71f) return asin2f(x);
    else if( x > 0.0f) return (M_PI_2-asin2f(sqrtf(1.0f-(x*x))));
    /* x < 0 or x is NaN */
    else return (asin2f(sqrtf(1.0f-(x*x)))-M_PI_2);
}