#include <math.h>

float acosf(float x){
    return (M_PI_2 - asinf(x));
}