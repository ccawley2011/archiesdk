#include "archie/SDKTypes.h"
#include <math.h>

float ceilf(float x){
    u32 xInt;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
    if(!isfinite(x) || (x == -0)) return x;
    
    xInt = (u32)fabsf(x);
    if(xInt == x) return x;
#pragma GCC diagnostic pop

    if(x>0) return (float)xInt+1;
    else return -(float)xInt;
}