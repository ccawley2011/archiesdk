#include <math.h>

float coshf(float x){
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
    if(x == -NAN) return x;
#pragma GCC diagnostic pop
    if(fabsf(x) > 89.41f) return INFINITY; /* hacky */
    return 0.5f * (expf(x) + 1.0f/expf(x));
}
