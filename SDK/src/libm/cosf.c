#include <math.h>

#include "archie/SDKTypes.h"

typedef union {
  u32 u;
  i32 i;
  float f;
} u_isf;

float cosf(float x){
    u32 pi_count, i;
    u_isf n_one_f;
    float taylor_series = 1.0;

    if(isinf(x)) return -NAN;
    x = fabsf(x);

    pi_count = (u32)(x / M_PI);
    x -= (float)pi_count * M_PI;

    for(i = 14; i > 0; i-=2){
        taylor_series = 1.0f - (x*x)/(float)(i*(i-1)) * taylor_series;
    }

    n_one_f.u = 0xBF800000;
    return (pi_count & 1) ? taylor_series * n_one_f.f : taylor_series;  
}