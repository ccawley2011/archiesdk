#include <math.h>

#include "archie/SDKTypes.h"

typedef union {
  u32 u;
  i32 i;
  float f;
} u_isf;

float logf(float x){
    u_isf xisf, bx;
    u32 ex;
    i32 t;
    
    /* edge cases */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
    if(x == 0.0f) return -INFINITY;
#pragma GCC diagnostic pop
    if(signbit(x)) return -NAN;
    if(!isnormal(x)) return x;

    xisf.f = x;

    bx.u = xisf.u;
    ex = bx.u >> 23;
    t = (i32)ex-(i32)127;
    bx.u = 0x3F800000 | (bx.u & 0x7FFFFF);
    x = bx.f;
    return -1.7417939f+(2.8212026f+(-1.4699568f+(0.44717955f-0.056570851f*x)*x)*x)*x+M_LN2*t;
}
