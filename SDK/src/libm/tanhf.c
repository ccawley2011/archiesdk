#include <math.h>

float tanhf(float x){
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
    if(x == -0.0f) return x;
#pragma GCC diagnostic pop
    if(fabsf(x) > 9.010111f) return(signbit(x)? -1.0f : 1.0f); /* hacky */
    return (expf(2*x) - 1.0f) / (expf(2*x) + 1.0f);
}