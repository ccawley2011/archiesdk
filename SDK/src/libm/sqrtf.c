#include <math.h>

#include "archie/SDKTypes.h"

float sqrtf(float x){
    float lo, hi, mid;
    u32 i;

    if(x < 0.0f) return -NAN;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
    if(x == -0.0f) return x;
#pragma GCC diagnostic pop

    /* Max and min are used to take into account numbers less than 1 */
    lo = MIN(1, x);
    hi = MAX(1, x);

    /* Update the bounds to be off the target by a factor of 10 */
    while(100.0f * lo * lo < x) lo *= 10.0f;
    while(0.01f * hi * hi > x) hi *= 0.1f;

    for(i = 0 ; i < 100 ; i++){
        mid = (lo+hi)/2;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
        if(mid*mid == x) return mid;
#pragma GCC diagnostic pop
        if(mid*mid > x) hi = mid;
        else lo = mid;
    }
    return mid;
}
