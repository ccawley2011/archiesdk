#include <math.h>

float fabsf(float x){
    return signbit(x)? -x : x;
}