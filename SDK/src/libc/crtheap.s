//Inserted at the end of the program; indictates where the program ends and heap begins.
//Used by crt0 to setup heap and by stdlib memory allocation routines with the OS_Heap SWI

.section .noinit
.global _heapStart
_heapStart:
    .space 4
