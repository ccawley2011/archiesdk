#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <math.h>

#include "archie/SDKTypes.h"

int (*_pputchar)(int);
int (*_pputs)(const char*);
u32 _printf_nc;

int fake_putchar(int character){
    return character;
}

int fake_puts(__attribute__((unused)) const char* ignored){
    return 1;
}

u8 do_printf_hex(u32 number, char alphaStart, int (*pputchar)(int)){
    u8 shift = 32;
    u8 cnt = 0;
    u8 digit;
    while(shift){
        shift -= 4;
        digit = (number>>shift)&0xf;
        if(digit) {
            ++cnt;
        } else if(!cnt) continue;
        pputchar(digit + ((digit < 10)? '0' : alphaStart-10));
    }
    return cnt;
}

u8 do_printf_unsigned(u32 number, int (*pputchar)(int)){
    unsigned char tmp[16];
    u8 curDigit = 0;
    u8 cnt;

    while(number > 9) {
        tmp[curDigit++] = '0' + (number % 10);
        number /= 10;
    }
    cnt = curDigit+1;
    pputchar('0' + number);
    while(curDigit) {
        pputchar(tmp[curDigit-1]);
        --curDigit;
    }
    return cnt;
}

void do_printf_hex_padding(u32 number, bool hashflag, bool rightjustify, char paddingSymbol, char alphaStart, i32 precision, i32 width, int (*pputchar)(int)){

    register int (*pputchar_bak)(int) = pputchar;
    i32 precisionbak = precision;
    i32 n;

    if(rightjustify) pputchar = fake_putchar;

do_printf_printHex:
    if(hashflag) {
        width -= 2;
        pputchar('0');
        pputchar(('x'-'a')+alphaStart);
    }

    n = do_printf_hex(number, alphaStart, fake_putchar); /* get count */
    while((precision - n) > 0) { /* pad */
        pputchar('0');
        --precision;
        --width;
    }
    width -= n;
    
    do_printf_hex(number, alphaStart, pputchar);

    if(rightjustify) {
        if(pputchar != pputchar_bak) {
            if(number == 0) ++width;    
            pputchar = pputchar_bak;
            precision = precisionbak;
            while((--width)>0) pputchar(paddingSymbol);
            goto do_printf_printHex;
        }
    } else while((width--)>0) pputchar('c');
}

void do_printf_number_padding(u32 number, bool neg, bool rightjustify, char posSign, i32 precision, i32 width, int (*pputchar)(int)){

    register int (*pputchar_bak)(int) = pputchar;
    i32 precisionbak = precision;
    i32 n;

    if(rightjustify) pputchar = fake_putchar;

do_printf_printNumber:
    if(neg) pputchar('-');
    else if(posSign) pputchar(posSign);

    n = do_printf_unsigned(number, fake_putchar); /* get count */
    while((precision - n) > 0) { /* pad */
        pputchar('0');
        --precision;
        --width;
    }
    width -= n;
    
    do_printf_unsigned(number, pputchar);

    if(rightjustify) {
        if(pputchar != pputchar_bak) {    
            pputchar = pputchar_bak;
            precision = precisionbak;
            while((width--)>0) pputchar(' ');
            goto do_printf_printNumber;
        }
    } else while((width--)>0) pputchar(' ');
}

void do_printf(const char* fmt, va_list* argp) {
    int (*pputchar_bak)(int);
    int (*pputs_bak)(const char*);

    bool special = false;
    u32 unsignedNum;
    i32 signedNum;
    double floatNum;
    char* str;
    bool neg;
    i32 precision = 0;
    bool customPrecision = false;
    i32 width = 1;
    bool rightjustify = true;
    bool hashflag = false;
    char posSign = 0;
    char paddingSymbol = ' ';
    const char* infString = "inf";
    const char* nanString = "nan";

    _printf_nc = 0;
    
    while(*fmt) {
        neg = false;
        if(special) {
            switch(*fmt){
                i32 precisionBak;
                double floatNumBak;
                
                case 'h':
                case 'l':
                case 'j':
                case 'z':
                case 't':
                case 'L':
                    /* ignored for now */
                    ++fmt;
                    continue;
                case '-':
                    rightjustify = false;
                    ++fmt;
                    continue;
                case '+':
                    posSign = '+';
                    ++fmt;
                    continue;
                case ' ':
                    posSign = ' ';
                    ++fmt;
                    continue;
                case '0':
                    paddingSymbol = '0';
                    ++fmt;
                    continue;
                case '#':
                    hashflag = true;
                    ++fmt;
                    continue;
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    width = atoi(fmt);
                    while(isdigit(*fmt)) ++fmt;
                    continue;
                case '*':
                    width = va_arg(*argp, u32);
                    ++fmt;
                    continue;
                case '.':
                    ++fmt;
                    if(*fmt == '*') {
                        precision = va_arg(*argp, i32);
                        ++fmt;
                    } else {
                        precision = abs(atoi(fmt));
                        while(isdigit(*fmt)) ++fmt;
                    }
                    customPrecision = true;
                    continue;

                case '%':
                    _pputchar('%');
                    break;
                case 'c':
                    if(rightjustify) while((--width)>0) _pputchar(' ');
                    _pputchar((char)va_arg(*argp, int));
                    if(!rightjustify) while((--width)>0) _pputchar(' ');
                    break;
                case 's':
                    str = va_arg(*argp, char*);
                    if(!precision) width -= strlen(str);
                    else width -= precision;
                    
                    if(rightjustify) while((width--)>0) _pputchar(' ');
                    
                    if(precision) {
                        while((precision--)>0) {
                            if(*str == 0) break;
                            _pputchar(*str++);
                        }
                    }
                    else _pputs(str);
                    
                    if(!rightjustify) while((width--)>0) _pputchar(' ');
                    break;
                case 'n':
                    *va_arg(*argp, i32*) = _printf_nc;
                    break;
                case 'p':
                    unsignedNum = va_arg(*argp, u32);
                    if(unsignedNum == 0) _pputs("(null)");
                    else do_printf_hex_padding(unsignedNum, true, rightjustify, paddingSymbol, 'a', precision, width, _pputchar);
                    break;
                case 'x':
                    unsignedNum = va_arg(*argp, u32);
                    do_printf_hex_padding(unsignedNum, hashflag, rightjustify, paddingSymbol, 'a', precision, width, _pputchar);
                    break;
                case 'X':
                    unsignedNum = va_arg(*argp, u32);
                    do_printf_hex_padding(unsignedNum, hashflag, rightjustify, paddingSymbol, 'A', precision, width, _pputchar);
                    break;
                case 'u':
                    unsignedNum = va_arg(*argp, u32);
                    do_printf_number_padding(unsignedNum, 0, rightjustify, 0,  precision, width, _pputchar);
                    break;
                case 'd':
                case 'i':
                    signedNum = va_arg(*argp, i32);

                    if((neg = (signedNum < 0))) signedNum = abs(signedNum);
                    if(neg || posSign) --width;

                    do_printf_number_padding(signedNum, neg, rightjustify, posSign, precision, width, _pputchar);

                    break;
                case 'F':
                case 'G':
                    infString = "INF";
                    nanString = "NAN";
                    __attribute__((fallthrough));
                case 'f':
                case 'g':
                    floatNum = va_arg(*argp, double);
                    if(!customPrecision) precision = 6;

                    if((neg = signbit(floatNum))) floatNum = -floatNum;
                    if(neg || posSign) --width;

                    pputchar_bak = _pputchar;
                    pputs_bak = _pputs;
                    precisionBak = precision;
                    floatNumBak = floatNum;
                    if(rightjustify) {
                        _pputchar = fake_putchar;
                        _pputs = fake_puts;
                    }

                do_printf_printFloat:
                    if(neg) _pputchar('-');
                    else if(posSign) _pputchar(posSign);
                    
                    if(isinf(floatNum)) {
                        _pputs(infString);
                        width -= 3;
                    } else if(isnan(floatNum)) {
                        _pputs(nanString);
                        width -= 3;
                    } else {
                        u32 decPart;
                        u32 mask = 1;
                        u32 tmp = precision;
                        u32 intPart = (u32)floatNum;
                        floatNum -= intPart;

                        while(tmp--) mask *= 10;
                        if(mask == 1) {
                            precision = 0;
                            mask = 0;
                        }
                        decPart = (u32)((floatNum*(double)mask) + 0.5);

                        if((intPart == 0) && (decPart >= (double)mask)) {
                            _pputchar('1');
                            --width;
                            decPart = 0;
                        } else width -= do_printf_unsigned(intPart, _pputchar);
                        
                        if((hashflag) || (precision)) {
                            _pputchar('.');
                            --width;
                        }
                        if(precision) {
                            mask /= 10;
                            while(decPart < mask) {
                                _pputchar('0');
                                --width;
                                --precision;
                                mask /= 10;
                            }
                            if(decPart) tmp = do_printf_unsigned(decPart, _pputchar);
                            else tmp = 0.0f;
                            precision -= tmp;
                            width -= tmp;
                            while((precision--)>0) {
                                _pputchar('0');
                                --width;
                            }
                        }

                    }

                    if(rightjustify) {
                        if(_pputchar != pputchar_bak) {    
                            _pputchar = pputchar_bak;
                            _pputs = pputs_bak;
                            precision = precisionBak;
                            floatNum = floatNumBak;
                            while((width--)>0) _pputchar(paddingSymbol);
                            goto do_printf_printFloat;
                        }
                    } else while((width--) > 0) _pputchar(' ');
                    break;
                default:
                    _pputchar('%');
                    _pputchar(*fmt);
                    break;
            }
            special = false;
        } else {
            if(*fmt == '%') {
                /* reset defaults */
                special = true;
                width = 1;
                precision = 0;
                customPrecision = false;
                rightjustify = true;
                hashflag = false;
                posSign = 0;
                paddingSymbol = ' ';
                infString = "inf";
                nanString = "nan";
            } else _pputchar(*fmt);
        }
        ++fmt;
    }

}