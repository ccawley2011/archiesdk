#include "archie/SWI.h"

int getchar(void){
    char result;
    asm volatile("  SWI " swiToConst(OS_ReadC) "\n\
                    mov %0, r0" : "=r"(result) : : "r0");
    return result;
}