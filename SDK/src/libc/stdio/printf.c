#include <stdio.h>
#include <stdarg.h>

#include "archie/SDKTypes.h"

extern int __puts(const char* string);
extern void do_printf(const char* fmt, va_list* argp);
extern int (*_pputchar)(int);
extern int (*_pputs)(const char*);
extern u32 _printf_nc;

int printf(const char *fmt, ...){
	va_list	listp;
    _pputchar = putchar;
    _pputs = __puts;

	va_start(listp, fmt);
	do_printf(fmt, &listp);
	va_end(listp);
    return _printf_nc;
}