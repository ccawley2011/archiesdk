#include <string.h>

#include "archie/SWI.h"
#include "archie/SDKTypes.h"

extern u32 _printf_nc;

int __puts(const char* string){
    _printf_nc += strlen(string);
    asm volatile("   MOV R0, %0\n \
            swi " swiToConst(OS_Write0) : : "r" (string) : "r0", "cc");
    return 1;
}

int puts(const char* string){
    __puts(string);
    return __puts("\r\n");
}
