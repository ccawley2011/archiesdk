#include <stdio.h>

char* gets(char* str){
    char* strBak = str;
    while(1) {
        char curchr = getchar();
        if((curchr == '\0') || (curchr == '\n')){
            *str = '\0';
            if(curchr == '\0') return NULL;
            else break;
        } else *str = curchr;
        ++str;
    }
    return strBak;
}
