#include "archie/SWI.h"
#include "archie/SDKTypes.h"

extern u32 _printf_nc;

int putchar(int character){
    ++_printf_nc;
    asm volatile("   MOV R0, %0\n \
            swi " swiToConst(OS_WriteC) : : "r" (character) : "r0", "cc");
    return character;
}
