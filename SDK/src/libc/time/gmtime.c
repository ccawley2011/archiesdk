#include <time.h>
#include <stdbool.h>

#include "archie/SDKTypes.h"

#define dow(y, m, d) \
  ((((((m)+9)%12+1)<<4)%27 + (d) + 1 + \
  ((y)%400+400) + ((y)%400+400)/4 - ((y)%400+400)/100 + \
  (((m)<=2) ? ( \
  (((((y)%4)==0) && (((y)%100)!=0)) || (((y)%400)==0)) \
  ? 5 : 6) : 0)) % 7)

extern bool __isleap(u32 yr);
extern u32 __months_to_days(u32 month);
extern i32 __years_to_days(u32 yr);
extern i32 __ymd_to_scalar(u32 yr, u32 mo, u32 day);
  
static void __scalar_to_ymd(i32 scalar, u32* pyr, u32* pmo, u32* pday) {
    u32 n;

    n = (u32) ((scalar * 400L) / 146097L);
    while (__years_to_days(n) < scalar) {
        n++;
    }
    for (n = (u32) ((scalar * 400L) / 146097L); __years_to_days(n) < scalar;)
        n++;                          /* 146097 == years_to_days(400) */
    *pyr = n;
    n = (u32) (scalar - __years_to_days(n - 1));
    if (n > 59) {                       /* adjust if past February */
        n += 2;
        if (__isleap(*pyr))
            n -= n > 62 ? 1 : 2;
    }
    *pmo = (n * 100 + 3007) / 3057;  /* inverse of months_to_days() */
    *pday = n - __months_to_days(*pmo);
    return;
}

static struct tm gmtime_tms;
struct tm* gmtime(const time_t* timer){
    u32 yr, mo, da;
    u32 secs;
    u32 days;

    days = *timer / (60L * 60 * 24);
    secs = *timer % (60L * 60 * 24);
    __scalar_to_ymd(days + __ymd_to_scalar(1970, 1, 1), &yr, &mo, &da);
    gmtime_tms.tm_year = yr - 1900;
    gmtime_tms.tm_mon = mo - 1;
    gmtime_tms.tm_mday = da;
    gmtime_tms.tm_yday = (i32) (__ymd_to_scalar(gmtime_tms.tm_year + 1900, mo, da)
                         - __ymd_to_scalar(gmtime_tms.tm_year + 1900, 1, 1));
    gmtime_tms.tm_wday = dow(gmtime_tms.tm_year + 1900, mo, da);
    gmtime_tms.tm_isdst = -1;
    gmtime_tms.tm_sec = (i32) (secs % 60);
    secs /= 60;
    gmtime_tms.tm_min = (i32) (secs % 60);
    secs /= 60;
    gmtime_tms.tm_hour = (i32) secs;
    return &gmtime_tms;
}