#include <time.h>

#include "archie/SDKTypes.h"
#include "archie/SWI.h"

clock_t clock(void){ /* note to self: remember to init in crt0 and update in the wimp_poll wrapper */

    u64 timerOutput = 0;
    asm volatile("mov r0, #1\n"
                 "mov r1, %0\n"
                 "swi " swiToConst(OS_Word)"\n"
                 :
                 : "r"(&timerOutput)
                 : "r0", "r1", "cc", "memory");

    return timerOutput; /* approximation of clocks since app start */
}