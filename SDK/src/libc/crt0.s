.arch armv2a
.file	"crt0.s"
.section	.init
.align 2
.global	_start
.syntax unified
.arm
.fpu softvfp

_start:
	adrl sp, riscos_ret //setup stack
	str lr, riscos_ret //write return address
	
	mov r0, #0
  	str r0, _atexit_funcs_p //reset to 0

	//setup heap
	mov r0, #-1
	mov r1, #-1
	swi 0x400ec //Wimp_SlotSize

	ldr r1, =_heapStart
	ldr r2, =_start
	subs r2, r1, r2
	subs r3, r0, r2
	mov r0, #0
	swi 0x1d //OS_Heap

	bl main
//fall through to exit
.global exit
exit:
	push {r0} //return code
	bl _atexit_call
	pop {r0}
.global abort
abort:
	ldr lr, riscos_ret
	mov pc, lr
.size _start, . - _start

.global _atexit_funcs_p
_atexit_funcs_p:
	.space 4
loadbearingvariable: 
	.space 4 //i don't know why but without this the stack doesn't work. off by 1?
stack_base:
    .space 1024
riscos_ret:
	.space 4

