#include <stdlib.h>
#include <string.h>

void* calloc(size_t nitems, size_t size){
    size_t totalSize = size*nitems;
    return memset(malloc(totalSize), 0, totalSize);
}