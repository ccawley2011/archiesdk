#include <stdlib.h>
#include <stdio.h>

#include "archie/SDKTypes.h"
#include "archie/SWI.h"

void* realloc(void *ptr, size_t size){
    
    u32* newPtr = (((u32*)ptr)-1);

    if(!ptr) return malloc(size);

    if(size <= 0) {
        free(ptr); 
        return NULL;
    }

    size += 4;
    asm volatile("mov r0, #4\n"
                 "ldr r1, =_heapStart\n"
                 "mov r2, %0\n"
                 "mov r3, %1\n"
                 "swi " swiToConst(OS_Heap)"\n"
                 "mov %0, r2\n"
                 : "+r"(newPtr) 
                 : "r"(size - *newPtr) /* <new size> - <old size> */
                 : "r0", "r1", "r2", "r3", "cc", "memory");

    *newPtr = size; /* set new size */
    return newPtr+1;
}