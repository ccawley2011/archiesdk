#include <stdbool.h>
#include <ctype.h>

#include "archie/SDKTypes.h"

extern bool _stdlib_isWhitespace(const char character);

float strtod(const char *str, char** endptr){
    float tmp = 0.0f;
    bool negative = false;

    /* nom whitespace :3 */
    while(_stdlib_isWhitespace(*str)) ++str;

    if(*str == '-') {
        negative = true;
        ++str;
    }

    /* todo: check for nan/inf */

    while(isdigit(*str)) {
        tmp = (tmp*10) + (*str - 48);
        ++str;
    }

    if(*str == '.') { /* if we have a decimal part */
        u32 length = 0;
        float tmp_decimal = 0.0f;
        ++str;

        while(isdigit(*str)) {
            tmp_decimal = (tmp_decimal*10) + (*str - 48);
            ++str;
            ++length;
        }
        while(length--) tmp_decimal /= 10;
        tmp = tmp + tmp_decimal;
    }

    if(negative) tmp = -tmp;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
    if(endptr) *endptr = (char*)str; /* blame the C standard for this one*/
#pragma GCC diagnostic pop
    return tmp;
}