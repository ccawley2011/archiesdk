#include "archie/SDKTypes.h"

static void (*_atexit_funcs[8])(void) = {0};
extern u32 _atexit_funcs_p;
void _atexit_call(void){
    if(!_atexit_funcs_p) return;
    do { _atexit_funcs[--_atexit_funcs_p](); } while(_atexit_funcs_p);
}

int atexit(void (*func)(void)){
    if(_atexit_funcs_p == 8) return 1;
    _atexit_funcs[_atexit_funcs_p++] = func;
    return 0;
}