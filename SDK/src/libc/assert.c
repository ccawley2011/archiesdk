#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

void __assert(const char* msg, const char* file, unsigned int line){
    printf("Assertion failed! %s in %s:%d! Aborting...", msg, file, line);
    abort();
}