#include <ctype.h>

int isprint(int character){
    return !iscntrl(character);
}