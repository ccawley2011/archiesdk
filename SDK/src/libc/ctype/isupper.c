int isupper(int character){
    return (character > 64) && (character < 91);
}