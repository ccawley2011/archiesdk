int isgraph(int character){
    return (character > 32) && (character != 127);
}