#include <ctype.h>

int ispunct(int character){
    return isgraph(character) && !isalnum(character);
}