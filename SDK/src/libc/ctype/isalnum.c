#include <ctype.h>

int isalnum(int character){
    return isalpha(character) || isdigit(character);
}