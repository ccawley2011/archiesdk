#include <string.h>

#include "archie/SDKTypes.h"

int strncmp(const char* str1, const char* str2, size_t n){
    while (n-- && (*str1 == *str2++)) if (!*str1++) return 0; 
    return *(const u8*)str1 - (n? *(const u8*)(--str2) : *(const u8*)str2);
}
