#include <string.h>

char* strchr(const char* str, int c){
    do {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
        if(*str == (char)c) return str; /* once again following the standard here */
#pragma GCC diagnostic pop
    } while(*str++);
    return NULL;
}
