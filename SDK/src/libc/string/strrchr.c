#include <string.h>

char* strrchr(const char* str, int c){
    char* strc = NULL;
    while(1){
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
        if(*str == (char)c) strc = str; /* once again following the standard here */
#pragma GCC diagnostic pop
        if(!*str) return strc;
    }
}