#include <string.h>

char* strncpy(char* dest, const char* src, size_t n){
    char* tmp = dest;

    while(n--){
        if(!(*dest++ = *src++)) {
            while(--n) *dest++ = '\0';
            break;
        }
    }

	return tmp;
}