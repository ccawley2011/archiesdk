#include <string.h>

size_t strcspn(const char* str1, const char* str2){
    size_t nChars = 0;
    while(1) {
        size_t i;
        size_t str2len = strlen(str2);
        for(i = 0; i < str2len; ++i){
            if(*str1 == str2[i]) return nChars;
        }
        ++str1;
        ++nChars;
    }
}