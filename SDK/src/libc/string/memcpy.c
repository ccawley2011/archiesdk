#include <string.h>

#include "archie/SDKTypes.h"

void* memcpy(void* dest, const void* src, size_t size){
    u8* cdest = (u8*)dest;
    const u8* csrc = (const u8*)src;
    
    while(size--) {
        *cdest = *csrc;
        ++cdest;
        ++csrc;
    }
    return dest;
}