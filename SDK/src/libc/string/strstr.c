#include <string.h>

char* strstr(const char* haystack, const char* needle){
    size_t n = strlen(needle);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
    while(*haystack) if(!strncmp(haystack++,needle,n)) return (haystack-1);
#pragma GCC diagnostic pop
    return 0;
}