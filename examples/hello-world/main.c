#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#include "archie/video.h"
#include "archie/keyboard.h"


void quit(){
    v_disableVSync();
    //flush last vsync
    v_waitForVSync();
}

void init(){ 
    v_disableTextCursor();
    v_enableVSync();

    atexit(quit); //register exit callback
}

int main(){
    
    init();
    
    puts("Hellorld! This is a basic C program\r\ncompiled with ArchieSDK running on\r\nAcorn Archimedes!\r\n\r\n");

    time_t curtime = time(NULL);
    printf("Today's date is \r\n%s\r\n\r\n", ctime(&curtime));

    printf("Here's the RISC OS char table\r\n\r\n    0 1 2 3 4 5 6 7 8 9 A B C D E F");
    for(u32 i = 0; i < 256; ++i) {
        if((i&0b1111) == 0) printf("\r\n%02lX: ", i);
        printf("%c ", isprint(i)? (char)i : ' ');
    }

	return 0;
}