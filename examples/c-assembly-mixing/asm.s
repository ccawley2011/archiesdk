//u32 asmPrintConst(void);
//prints a string constant defined in asm.s and returns 0xBABABABA
.global asmPrintConst
asmPrintConst:
//backup return address
    push {lr}
//call puts(asmPrintHelloStr)
    adr r0, asmPrintHelloStr
    bl puts
//return 0xBABABABA
    ldr r0, =#0xBABABABA
    pop {pc}


//void asmPrintOneArg(const char* text);
//prints a string constant passed as argument and returns nothing
.global asmPrintOneArg
asmPrintOneArg:
//backup return address
    push {lr}
//text is passed in r0
//function calls expect the first arg in r0
//so we just call puts
    bl puts
    pop {pc}


//void asmPrintTwoArgs(const char* text1, const char* text2); 
//prints two string constants and returns nothing 
.global asmPrintTwoArgs
asmPrintTwoArgs:
//backup return address
    push {lr}
//text2 is in r1, back it up
    push {r1}
//text1 is passed in r0
//function calls expect the first arg in r0
//so just call puts
    bl puts
//pop text2 from the stack and call puts
    pop {r0}
    bl puts
//return
    pop {pc}


//void asmPrintReturnValue(void);
//calls returnConstString and prints whatever that returned
.global asmPrintReturnValue
asmPrintReturnValue:
    push {lr}
//call returnConstString
    bl returnConstString
//returnConstString returned a const char* in r0
//puts expects a const char* and the first argument goes in r0, so call puts
    bl puts
//return
    pop {pc}


//void asmCallCFunctions(void);
//calls a bunch of C functions
.global asmCallCFunctions
asmCallCFunctions:
    push {lr}

    adr r0, cPrintOneArgStr
    bl cPrintOneArg
    
    adr r0, cPrintTwoArgsStr1
    adr r1, cPrintTwoArgsStr2
    bl cPrintTwoArgs

    pop {pc}


asmPrintHelloStr:
    .string "printHello"
cPrintOneArgStr:
    .string "cPrintOneArg"
cPrintTwoArgsStr1:
    .string "cPrintTwoArgs1"
cPrintTwoArgsStr2:
    .string "cPrintTwoArgs2"
