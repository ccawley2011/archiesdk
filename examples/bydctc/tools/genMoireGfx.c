#include "stdio.h"
#include "math.h"
#include "stdint.h"

//generates that funky circle gfx

int main(void){
    uint8_t moireGfx[320*640];

    for(int x = 0; x < 640; ++x) {
        for(int y = 0; y < 320; ++y) {
            float dx = (x - 320) * (x - 320);
            float dy = (y - 160) * (y - 160);
            moireGfx[(y*640)+x] = (!((((uint32_t)sqrtf(dx + dy)) >> 4) & 1)) * 255;
        }
    }

    printf("#include \"archie/SDKTypes.h\"\n\nu8 moireGfx[320*640] = {");
    for(int i = 0; i < 320*640; ++i) printf("%u,", moireGfx[i]);
    printf("};\n\n");

    return 0;
}