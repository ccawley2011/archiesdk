#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "sourceData/font.h"

typedef unsigned int u32;

//converts (or packs in this case I guess) the font gfx from chars to u32s

int main(){

    printf("#include \"archie/SDKTypes.h\"\n\nu32 fontData[] = {");
    for(u32 curPixel = 0; curPixel < width*height; curPixel+=width*3){
        for(u32 x = 0; x < width; ++x){
            printf("%u, ", (header_data[curPixel]? 255 : 0) | ((header_data[curPixel+width]? 255 : 0)<<8) | ((header_data[curPixel+(width*2)]? 255 : 0)<<16) | ((header_data[curPixel+(width*3)]? 255 : 0)<<24));
            ++curPixel;
        }
    }
    printf("};");


}