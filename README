###############################################################################

        _____   _____   _____ _     _  __  ______  _____ _____  __   __
       / ___ \ / ___ \ /  ___| |   | ||  ||  ____|/  ___|  __ \|  | /  |
      | |___| | |   \_|  /   | |___| ||  || |____|  |__ | |  | |  |/  /
      |  ___  | |     | |    |  ___  ||  ||  ____|\___ \| |  | |     |
      | |   | | |     |  \___| |   | ||  || |____ ___|  | |__| |  |\  \
      |_/   \_|_|      \_____|_|   |_||__||______|_____/|_____/|__| \__|
                                  Release 1

###############################################################################

PLEASE READ THE ENTIRE DOCUMENT BEFORE TRYING TO COMPILE/RUN ANYTHING

Hiya! Welcome to the first release of ArchieSDK, a C SDK for Acorn Archimedes 
machines running RISC OS/Arthur.

This document will go over a few things:

    -C toolkit implementation details
    -Building and setting up the SDK
    -Building a Hello World app using the SDK

This is the first release, so there will be a few bugs! If you find any,
please report them to us so we can fix them for the next release!

There were some difficulties getting GCC working with RISC OS 3.11 on the A30x0
series and so this SDK has a few quirks you'll have to keep in mind.
We really think you should take the time to read the entirety of this document
before proceeding.

We have a discord server. It's not super active at the moment but if you have
any questions or wish to report bugs, feel free to join it. The server also has
an announcements channel for SDK updates and a show and tell channel if you
wish to show your projects off.
You can join the server at https://discord.gg/nbHKm2SQ9g


1. C toolkit implementation details:

We're using GCC 8.5.0, it was released in May of 2021 and was the last version
to support ARM2. With newer versions having completely removed ARMv<4 support, 
it is unlikely that a more modern compiler will ever be released for Archimedes

Right now this SDK is Linux only. It might be compatible with Macs, but that's
untested. If you're a Windows user, this was developed under WSL2 so that
should work just fine.

The SDK only supports C as of this release. The C++ standard lib is a lot more
complex/bloated than the C one so we're probably not going to implement it 
ourselves. If enough people are interested it would be possible to
add in basic C++ support and provide wrappers for the C stdlib in a future
release.

The C standard library implementation used in this project differs slightly
from the official specifications which can cause some unintended side effects.
We've listed all the bugs and quirks we know of below, but there's probably a
few more we've missed!

1.a KNOWN BUGS/QUIRKS:

Some of the issues you might encounter while coding will be because of SDK
quirks, so we highly recommend that you read through this section in detail
as it might save you a few debugging headaches.

-Global variables are all messed up on a 2nd/3rd/4th/nth run of my app!

    RISC OS caches executables in RAM and doesn't expect them to get
    modified at run time. If you run an app built with the SDK twice without
    restarting or rebuilding, the non-const global variables don't get reset.
    Meaning that if you have a global variable, on a second run it'll have
    whatever value it had at the end of the first run.

    There's a few ways to fix this. You could initialise your global variables
    at run time. You could detect a second run by checking the state of a
    global flag variable and exit early with a message. Or you could avoid
    global variables altogether. This may get fixed in a future release.


-The C standard library is missing a lot of headers/functions!
-This C standard library function is a bit different!
-I passed --std=c89 to gcc but headers still define c99 functions/features!

    We had to write our own implementation of the standard library as we could
    not find a generic one suitable for this project. It's incomplete and a bit
    unique at times. There are lots of quirks, most of which are described
    below. Still, if you find something that is very wrong or different from
    the standard it might be a bug. So feel free to contact us to let us know!
    If you want to know the general philosophy behind a lot of this, we just
    looked at C stdlib headers and implemented what felt important or useful.
    However, one big feature the SDK is missing right now is the ability to
    toggle features and functions on and off based on the C standard you
    choose. This could be addressed in a future release but it's low priority.


-The C standard library functions are too slow!

    Our implementation prioritises accuracy first, size second and speed third.
    It's provided for reference and to make prototyping easier/faster. While we
    tried to make it fast, this first draft was done in a week. If you have 
    faster routines that are just as (or more) accurate, feel free to send 
    them! We'd be more than happy to merge them in for future releases.


-Standard math functions use floats instead of doubles!

    Normally, standard math functions use double precision and offer an
    alternative function that uses float. An example would be
    "double cos(double x)" which is offered alongside "float cosf(float x)"
    (note the f in the function name). Both of these functions do the same job
    of calculating the cosine of a number "x" but the first uses doubles which
    are far more expensive to compute.

    Most people don't know this and just call "cos" not knowing it uses a
    double, which, as you can guess, would be a very expensive mistake to make
    on an old Archimedes. So one of the small liberties that we took while
    writing the standard library was to use floats by default instead of
    doubles for the functions in math.h. For example, instead of
    "double cos(double x)", we offer "float cos(float x)". We still offer 
    "cosf" as well for compatibility, but it just calls "cos" internally.

    This change helped reduce code size and complexity a bit as we didn't
    need two different implementations for both standard math functions and
    their float counterparts. However, this means that if you want to use
    math functions with doubles on the Archimedes, you can't right now.
    We might implement them in the future, maybe alongside a define to select
    accuracy over performance, but for this release, math functions only use
    floats.


-make/gcc keeps warning me about floats getting converted to doubles!
-Passing a float value to printf generates warnings!

    As said previously, double precision floats are way more expensive to
    use on the Archimedes than regular floats. The issue is that it is easy to
    accidentally use doubles in your maths, so we enabled a warning to warn
    the user every time a float gets implicitly converted to a double.
    One way to accidentally use doubles is to forget the 'f' suffix to your
    float constants. "1.234" will be treated as a double, "1.234f" will be
    treated as a float. However, this means that if you do end up using doubles,
    either in your maths or in a function call, unless you explicitly cast
    your floats you will get a warning. "printf" is one such function that
    uses doubles internally, so every time you pass a float to it the float
    gets converted to a double implicitly. This causes a warning. You can
    just explicitly cast your float to double to remove the warning.
    You can also remove the warning from "config.mk", but we recommend against
    that.


-printf ignored my scientific notation specifier! (%E/%e)

    Scientific notation specifiers are not implemented yet.


-printf ignored my size specifiers!
-Using printf with a uint64_t value doesn't work!

    Size specifiers are not implemented yet. As of now all arguments are 
    treated as uint32_t. Size specifiers will likely be implemented in a future
    release.


-File operations are not implemented!

    Those are low priority for now but will likely be implemented in the next
    release.


2. Building and setting up the SDK

Now that you've been warned about the quirks, let's get started with building
and setting up the SDK.

As of now the only dependencies are wget, tar, make and gcc. Those come
pre-installed on most distros and gcc should be fairly easy to install with
your package manager.

Assuming that you now have all the dependecies installed, open a new terminal
and cd into the folder that contains this README. Now run "./build.sh".

"build.sh" is a very basic script so if something goes wrong during the build
it should be easy to fix. But if you run into some weird issues, feel free to
contact Targz (.tgz on discord) for help.

Once the build is complete, there should be a new folder named tools.
Here's what the tools folder should look like:

- <tools>
    - <arm-archie>
    - <bin>
    - <binutils-master>
    - <binutils-build>
    - <gcc-master>
    - <gcc-build>
    - <include>
    - <lib>
    - <libexec>
    - <share>
    - binutils.tar.gz
    - gcc.tar.gz

If you're missing a few folders/files, something might have gone wrong during
the build. You should delete the entire tools folder and start over.
If not, you are now the proud owner of a brand new fancy ArchieSDK! :)

Make sure to export the ARCHIESDK path in your environment variables as
specified at the end of build. Otherwise, you won't be able to compile any
projects.

2.a Post build tips

We offer a variable in "config.mk" named ARCHIECOPYPATH which is used as a
destination folder to copy your built executable to. The default value is '.'
which skips the copy but we implemented it so you can automatically copy the
executable to your hostfs, floppy drive or any other folder. To do this, 
just replace the default value with any valid path of your choice.

If you wish to save some space, you can delete the two tarballs
("binutils.tar.gz" and "gcc.tar.gz") as well as the binutils-master,
binutils-build, gcc-master and gcc-build folders. These are no longer necessary
and are quite big.

3. Building a Hello World app using the SDK

This SDK comes with a few of examples: a basic Hello World, a c/asm mixing 
example and a slightly more complicated intro we made for fun while testing
the SDK. We have also provided the source for the programs we wrote while 
testing the libc implementation. Feel free to use any of these as templates for
your programs. We recommend just copying the "examples/hello-world" folder
somewhere and using that as a base.

Building should be as easy as running "make" in your project folder.

Have fun now! :)


Credits:

    Tara "Targz" Colin - Wrote C runtime, stdlib implementation, SDK tools,
                         examples, libarchie, build scripts, this README.

    Enfys "TôBach" Castle-Roper - Help with libarchie funcs, ARM assembly and
                                  RISC OS SWIs. Also made the Radical ASCII 
                                  Art logo at the top of this README.

    Octavia "Kitsu" Lea - Help with maths, debugging, stdlib implementation and
                          this README. Also cooked targz a nice quesadilla :3

    Lex Bailey - Help with licensing. Thanks again for taking the time to 
                 explain everything.

    The Arculator devs - The amazing Arculator emulator without which this
                         project would not exist. (Seriously, Archimedeses are
                         way too expensive on ebay)

Greetz to:
    Bitshifters, Slipstream, Torment, Desire, Poo-Brain, TUHB, RiFT, Rabenauge,
    Marquee Design, Field-FX, stardot, Hooy-Program, CSC, AttentionWhore,
    Logicoma, 5711, SVATG, Haujobb, SimonTime and you!!
